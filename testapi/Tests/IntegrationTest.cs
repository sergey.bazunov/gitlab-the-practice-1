using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using webapi.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System;

namespace testapi;

public class IntegrationTest : IClassFixture<WebApplicationFactory<Program>> 
{
    //private readonly HttpClient _client = new HttpClient();

    public IntegrationTest()
    {   
        // var application = new WebApplicationFactory<Program>()
        // .WithWebHostBuilder(builder =>
        // {
            
        // });     
        // _client = application.CreateClient();
    }
    
    [Fact]
    public void WeatherForecast_TestController()
    {        
        var logger = Mock.Of<ILogger<WeatherForecastController>>();
        var controller = new WeatherForecastController(logger);
        // Act
        var result = controller.Get();

        // Assert
        var viewResult = Assert.IsType<webapi.WeatherForecast[]>(result);
    }

}