using webapi.Controllers;
using AutoFixture;
using AutoFixture.AutoMoq;
using Xunit;
using System.Collections;

namespace testapi;

public class UnitTest
{

    private readonly WeatherForecastController _Controller;

    public UnitTest()
    {
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        _Controller = fixture.Build<WeatherForecastController>().OmitAutoProperties().Create();
    }

    [Fact]
    public void WeatherForecastController_Result_OK()
    {
        var response = _Controller.Get();
        Assert.NotNull(response);
    }
}